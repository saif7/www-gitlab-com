---
layout: handbook-page-toc
title: "Observation Management Procedure"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Observation Management Procedure

## Purpose

Observations, also commonly referred to as findings, exceptions or deficiencies, refer to any [Tier 3](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html#scope) information system risks that are identified as an output of compliance operations or other mechanisms by team members, such as self-identification of a system specific risk.

## Scope

These risks identified at the information system or business process levels. The volume and granularity of these Tier 3 risks make it inappropriate to track via the [GitLab Risk Register](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html) and so this observation management process will guide team-members on how to track these Tier 3 risks.

## Roles and Responsibilities

| Role | Responsibility|
| ---- | ------ |
| Security Compliance | Responsible for executing [Security control tests](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html#) to determine test of design and test of operating effectiveness of Security and IT general controls. |
| Internal Audit | Responsible for executing [Internal Audit control tests](https://about.gitlab.com/handbook/internal-audit/sarbanes-oxley/#sarbanes-oxley-section-404-management-testing-plan) to determine test of design and test of operating effectiveness of all internal controls as required by audit plan. |
| Control Owner | Responsible for designing and implementing remediation plans in order to meet legal and regulatory requirements. |

## Observation Phases Overview

```mermaid
graph TD;
  A[Identified] --> B[Assigned];
  B --> C[Remediation in progress];
  B --> D[Ignored or Invalid];
  C --> F[Resolved];
```

## Procedure

The following phases walk through the observation lifecycle.

### Identifying Observations

Observations can be identified in the following ways:
1. Internal audit activities
1. Security control testing
1. Third Party Security Assessments
1. Customer Assurance Activities
1. External audits 
1. Third party application scanning (BitSight)
1. Ad-hoc issues

### Recording Observations

The audit team will validate the observation with the control owner then create all confirmed observations to ensure uniformity in the process. Each observation has both a GitLab Issue (for control owners) and a mirrored ZenGRC Issue (for audit team). Each observation will be assigned a [risk rating](https://julia-lake-master-patch-41588.about.gitlab-review.app/handbook/engineering/security/security-assurance/security-compliance/observation-management.html#observation-risk-ratings), which should drive the priority of remediation.

If multiple observation issues relate to the same root cause or are blocked by the same component of work, these issues will be connected together into an Epic in order to more clearly see how multiple observations issues are connected. 

To ensure transparency accross the organization, Security Compliance documents observations in their [Observation Management project](https://gitlab.com/gitlab-com/gl-security/compliance/observation-management). A list of observation Epics can be found [here](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/epics?label_name%5B%5D=Observation+Epics).

### Observation Risk Ratings

Tier 3 information system risk ratings are based off the [STORM risk rating methodology](/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html#risk-factors-and-risk-scoring).

Risk Rating = Likelihood x Impact

#### Determining the likelihood

At GitLab, observations will be rated based on the likelihood the observation has of recurring and/or the frequency that the control has seen observations. The criteria used to assess this likelihood can be found in the Likelihood Table below. Note that there are two different definitions for each likelihood rating level:

- **Control Observation**: This criteria is utilized to rate observations identified as an output of control testing (e.g. where control testing performed internally by Internal Audit and/or Security Compliance has failed). The assumption of the Likelihood Table is to consider observations individually rather than in aggregate (i.e if 2 similar observations occur against a single test of a sample of 25, the failure rate is 8% and would be scored a 3. The control does not need to be tested multiple times in the current year or prior 9 months with an observation each time to meet the requirement for a score of 3). 
- **Information System Risk (Tier 3)**: This criteria is utilized to score the likelihood of an information system being exploited (e.g. insufficient encyrption mechanisms for the storage of data within `[System Name]` result in the unintentional exposure/leakage of this information to the public)

##### Likelihood Table

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-za92{background-color:#FFF2CC;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-vknm{background-color:#EFEFEF;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Qualitative Score</span></th>
    <th class="tg-vknm" colspan="2"><span style="font-weight:bold;background-color:#EFEFEF">Description</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-cly1" colspan="2"><b>Control Observation: </b>The observation noted is considered to be a one off occurence for the control as a result of extenuating circumstances. It is unlikely to occur again once remediated.<br><br><b>Information System Risk: </b>Theoretically impossible and/or requires significant technical expertise for the risk to be exploited.</td>
  </tr>
  <tr>
    <td class="tg-za92"><span style="background-color:#FFF2CC">2</span></td>
    <td class="tg-cly1" colspan="2"><b>Control Observation: </b>The observation was identified as a result of management's oversight on the control and may potentially occur again. This is the only observation associated with the control in the current fiscal year or prior 9 months, whichever is longer.<br><br><b>Information System Risk: </b>Even with technical expertise, it is somewhat difficult to exploit the risk.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
    <td class="tg-cly1" colspan="2"><b>Control Observation: </b>The control has had multiple observations in the current fiscal year or prior 9 months, whichever is longer.<br><br><b>Information System Risk: </b>Minimal expertise is required to exploit the risk.</td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">4</span></td>
    <td class="tg-0lax" colspan="2"><span style="font-style:normal"><b>Control Observation: </b>The control has observations that have persisted and continue to occur year to year </span><span style="font-weight:bold;font-style:normal">AND/OR</span><span style="font-style:normal"> the observation noted is associated with the design of the control.<br><br><b>Information System Risk: </b>The risk can be easily exploited and does not require any technical expertise.</span></td>
  </tr>
</tbody>
</table>

#### Determining the observation impact

In addition to applying a qualitative scoring factor for likelihood, all observations need to be evaluated for the impact they would have to GitLab at the organization level and/or the compliance impact (if applicable). The criteria and qualitative scores for assessing the impact of an observation can be found in the Impact Scoring Table below. The **highest** rating in any field is the final impact score of the observation so as to approach observations in a more conservative manner (i.e if all fields are rated at a value of 2 except Remediation Effort which is scored a 3, the final impact score would be a 3).

**Important Note**: Team members who are leveraging the impact scoring criteria below may judgementally select the impact factors most relevant to them. Internal Audit and Security Compliance utilize all columns when scoring observations identified as part of controls testing because there may be specific impacts to external compliance audit requirements as a result of these findings. Any information system risk identified outside of control testing may utilize the columns that are most relevant.

##### Impact Scoring Table

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-za92{background-color:#FFF2CC;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-vknm{background-color:#EFEFEF;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Qualitative Score</span></th>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">External Audit Impact</span></th>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Remediation Effort</span></th>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Financial Impact</span></th>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Legal &amp; Regulatory Impact</span></th>
    <th class="tg-vknm"><span style="font-weight:bold;background-color:#EFEFEF">Stakeholder/ ICOFR (Internal Controls Over Financial Reporting)&nbsp;&nbsp;Impact </span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-cly1">The observation would not lead to an adverse audit opinion.</td>
    <td class="tg-cly1">The observation was related to extenuating circumstances and requires simple reinforcement of policy/process, no additional management oversight required. </td>
    <td class="tg-cly1">Observation has potential financial impact which results in loss / misstatement of up to $25K.</td>
    <td class="tg-cly1">The observation would not lead to major action by a regulator. </td>
    <td class="tg-cly1">The observation has minimal impact on all stakeholders (internal and external)</td>
  </tr>
  <tr>
    <td class="tg-za92"><span style="background-color:#FFF2CC">2</span></td>
    <td class="tg-cly1">The observation would likely not lead to an adverse audit opinion because it is an isolated occurrence.</td>
    <td class="tg-cly1">The observation has remediation effort that requires oversight/support at the management level.</td>
    <td class="tg-cly1">Observation has potential financial impact which results in loss / misstatement between $25K to $250K.</td>
    <td class="tg-cly1">The observation could lead to minor regulatory action.</td>
    <td class="tg-cly1">The observation impacts internal stakeholders and or could lead to financial misstatements, if not addressed on time. </td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
    <td class="tg-cly1">The observation is likely to result in an adverse audit opinion if a full sample for remediation testing cannot be provided. </td>
    <td class="tg-cly1">The observation has remediation effort that requires oversight/support at the director level.</td>
    <td class="tg-cly1">Observation has potential financial impact which results in loss / misstatement between $250K to $500K.</td>
    <td class="tg-cly1">The observation could lead to an investigation or regulatory action.</td>
    <td class="tg-cly1">The observation impacts internal and external stakeholders. It requires attention of Executives and Board. </td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">4</span></td>
    <td class="tg-cly1">The observation will result in an adverse audit opinion. </td>
    <td class="tg-cly1">The observation has remediation effort that requires oversight/support at the Executive level.</td>
    <td class="tg-cly1">Observation has potential financial impact which results in loss / misstatement above $ 500K</td>
    <td class="tg-cly1">The observation could directly result in major regulatory action against GitLab.</td>
    <td class="tg-cly1">The observation impacts internal and external stakeholders. It requires attention of Executives and Board and could impact management assertion in 10Q / 10-K.</td>
  </tr>
</tbody>
</table>

#### Determining the Observation Risk Rating

In order to arrive at a final observation risk rating, the likelihood and impact scores of an observation are multiplied together. The final score determined will determine whether or not the observation is a LOW, MODERATE, or HIGH risk observation using the Observation Risk Rating Table

##### Observation Risk Rating Table

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-319z{background-color:#666;color:#FFF;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-4bh4{background-color:#CFE2F3;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-mgvv{background-color:#B4A7D6;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-319z" colspan="5"><span style="font-weight:bold;color:#FFF;background-color:#666">Observation Risk Matrix</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-cly1"></td>
    <td class="tg-4bh4" colspan="4"><span style="font-weight:bold;background-color:#CFE2F3">Impact Score</span></td>
  </tr>
  <tr>
    <td class="tg-mgvv"><span style="font-weight:bold;background-color:#B4A7D6">Likelihood</span></td>
    <td class="tg-4bh4"><span style="font-weight:bold;background-color:#CFE2F3">1</span></td>
    <td class="tg-4bh4"><span style="font-weight:bold;background-color:#CFE2F3">2</span></td>
    <td class="tg-4bh4"><span style="font-weight:bold;background-color:#CFE2F3">3</span></td>
    <td class="tg-4bh4"><span style="font-weight:bold;background-color:#CFE2F3">4</span></td>
  </tr>
  <tr>
    <td class="tg-mgvv"><span style="font-weight:bold;background-color:#B4A7D6">1</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">3</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
  </tr>
  <tr>
    <td class="tg-mgvv"><span style="font-weight:bold;background-color:#B4A7D6">2</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">6</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">8</span></td>
  </tr>
  <tr>
    <td class="tg-mgvv"><span style="font-weight:bold;background-color:#B4A7D6">3</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">3</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">6</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">9</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">12</span></td>
  </tr>
  <tr>
    <td class="tg-mgvv"><span style="font-weight:bold;background-color:#B4A7D6">4</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">8</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">12</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">16</span></td>
  </tr>
</tbody>
</table>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-319z{background-color:#666;color:#FFF;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-nrix{text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-319z" colspan="2"><span style="font-weight:bold;color:#FFF;background-color:#666">Observation Risk Thresholds</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">LOW</span></td>
    <td class="tg-nrix">1 - 3</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">MODERATE</span></td>
    <td class="tg-nrix">4 - 9</td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">HIGH</span></td>
    <td class="tg-nrix">12 - 16</td>
  </tr>
</tbody>
</table>

### Additional Considerations Specific to Control Observations

The procedures outlined in the preceding sections below are used specifically by Internal Audit and Security Compliance. Team members utilizing the observation management program for rating information system risks outside of control testing activities will not need to engage in the procedures below.

#### Determining the Individual Control Health & Effectiveness Rating (CHER)

The importance of risk rating each control observations comes into play when making a final determination on how to establish a control's Control Health & Effectiveness Rating (CHER). CHER ratings on a sliding scale outside of the typical effective/ineffective rating used for compliance, allow for clearer communication and prioritization with broader audiences outside of compliance functions and allows non-compliance stakeholders the ability to view how observations impact the control environment.

CHER provides a qualitative value of a control's effectiveness that is used as an input for various processes within the Risk Management Program. When needing to report to management, these quantitative values are translated to qualitative terms: Fully Effective, Substantially Effective, Partially Effective, Largely Ineffective, Ineffective. Refer to the CHER Quantitative vs. Qualitative Terms and Definitions Table below for a mapping of CHER to it's definition and the related qualitative term and definition. Use the rating determined by completing the observation risk rating with likelihood and impact scores and applying that risk rating into the table below (i.e if a control has 1 low risk observation per the Observation Risk Rating table, the CHER for that control would be a 2 (Substantially Effective)).

##### CHER Quantitative vs. Qualitative Terms and Definitions (For individual controls)

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-wa1i{font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-w80k{background-color:#B4A7D6;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-wa1i"><span style="font-weight:bold">Quantitative Value</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">Quantitative Definition</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">CHER Qualitative Term</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">Qualitative Definition</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-cly1">The control has no outstanding HIGH, MODERATE, or LOW risk observations open.</td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Fully Effective</span></td>
    <td class="tg-cly1">Nothing more to be done except review and monitor existing controls. Controls are well designed for the risk, and address the root causes. Management believes they are effective and reliable at all times.</td>
  </tr>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
    <td class="tg-cly1">There are no outstanding HIGH or MODERATE risk observations associated with the control, but there are some LOW risk observations that are open</td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Substantially Effective</span></td>
    <td class="tg-cly1">Most controls are designed correctly and in place and effective. Some more work to be done to improve operating effectiveness or there are doubts about operational effectiveness and consistent reliability.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
    <td class="tg-cly1">There are no outstanding HIGH risk observations associated with the control, but there is a single open MODERATE (below 9 rating) risk observation and any number of LOW risk observations.</td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Partially Effective</span></td>
    <td class="tg-cly1">Design of controls is largely correct and they treat most of the root causes of the risk, however they are not currently operating very effectively.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
    <td class="tg-0lax"><span style="font-style:normal">There are no outstanding HIGH risk observations associated with the control, but there are </span><span style="font-weight:bold;font-style:normal">multiple</span><span style="font-style:normal"> open MODERATE (below 9 rating) risk observations OR a </span><span style="font-weight:bold;font-style:normal">single</span><span style="font-style:normal"> open MODERATE risk observation with a 9 rating. There can be any number of LOW risk observations.</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Largely Ineffective</span></td>
    <td class="tg-cly1">Significant control gaps. Either controls do not treat root causes or they do not operate at all effectively.</td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">5</span></td>
    <td class="tg-cly1">There are outstanding HIGH risk observations associated with the control.</td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">Ineffective</span></td>
    <td class="tg-cly1">Practically no credible control. Management has almost no confidence that any degree of control is being achieved due to poor control design or very limited operational effectiveness.</td>
  </tr>
  <tr>
    <td class="tg-w80k"><span style="background-color:#B4A7D6">0</span></td>
    <td class="tg-cly1">The control is not yet implemented.</td>
    <td class="tg-w80k"><span style="background-color:#B4A7D6">Control Not Implemented</span></td>
    <td class="tg-cly1">Control is not implemented and this is expected. This is different from a control gap because of the awareness around the control and the intentional exclusion of the control from being a key control in the environment. There are other sufficient controls to secure the environment in place.</td>
  </tr>
</tbody>
</table>

#### System Health Rating - Quantitative vs. Qualitative Terms and Definitions

CHER is assigned on a control by control basis but in instances where we want to report on system health, the ratio of high risk observations to the number of applicable controls that were assessed against the system is determined. That ratio is used to determine the system health rating from the following table: 

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-wa1i{font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-wa1i"><span style="font-weight:bold">Ratio of CHER rating to applicable controls assessed</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">System Health Rating Value</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Between 0% and 5% of controls = CHER 2,3,4,5,0</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
  </tr>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Between 5% and 35% of controls = CHER 2,3,4,5,0</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Greater than 35% up to 65% of controls = CHER 2,3,4,5,0</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Greater than 65% up to 85% of controls = CHER 2,3,4,5,0</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">Greater than 85% of controls = CHER 2,3,4,5,0</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">5</span></td>
  </tr>
</tbody>
</table>

Refer to the System Effectiveness Rating Table below for a mapping of averaged CHERs to the qualitative term and definition that can be used to report on system health/effectiveness. Note that when using this table the final average of CHER values should be rounded **up** to the nearest quantitative value to determine the CHER for the system (i.e if average of all CHER's equals 2.3, the final CHER for the system would be rounded up to a 3)

##### System Health Rating Table

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-wa1i{font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-wa1i"><span style="font-weight:bold">System Health Rating Value</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">System Health Rating Qualitative Term</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">Qualitative Definition</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Fully Effective</span></td>
    <td class="tg-cly1">Nothing more to be done except review and monitor the existing controls. Controls are well designed for the risk, and address the root causes. Management believes they are effective and reliable at all times.</td>
  </tr>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Substantially Effective</span></td>
    <td class="tg-cly1">Most controls are designed correctly and are in place and effective. Some more work to be done to improve operating effectiveness or management has doubts about operational effectiveness and reliability.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Partially Effective</span></td>
    <td class="tg-cly1">While the design of controls may be largely correct in that they treat most of the root causes of the risk, they are not currently operating very effectively.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Largely Ineffective</span></td>
    <td class="tg-cly1">Significant control gaps. Either controls do not treat root causes or they do not operate at all effectively.</td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">5</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">Ineffective</span></td>
    <td class="tg-cly1">Virtually no credible control. Management has no confidence that any degree of control is being achieved due to poor control design or very limited operational effectiveness.</td>
  </tr>
</tbody>
</table>

#### Control Family Effectiveness Rating - Quantitative vs. Qualitative Terms and Definitions

CHER is assigned on a control by control basis but in instances where we want to report on control family effectiveness, the CHER for each of the individual underlying controls in a control family can be averaged to provide a more holistic view. Refer to the Control Family Effectiveness Rating Table below for a mapping of averaged CHERs to the qualitative term and definition that can be used to report on control family health/effectiveness. Note that when using this table the final average of CHER values should be rounded **up** to the nearest quantitative value to determine the CHER for the control family (i.e if average of all CHER's equals 2.3, the final CHER for the control family would be rounded up to a 3).

##### Control Family Effectiveness Rating Table

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-cly1{text-align:left;vertical-align:middle}
.tg .tg-wa1i{font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-obmi{background-color:#D9EAD3;text-align:center;vertical-align:middle}
.tg .tg-yfns{background-color:#F4CCCC;text-align:center;vertical-align:middle}
.tg .tg-qjco{background-color:#FCE5CD;text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-wa1i"><span style="font-weight:bold">Quantitative Value</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">Control Family Effectiveness Rating Qualitative Term</span></th>
    <th class="tg-wa1i"><span style="font-weight:bold">Qualitative Definition</span></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">1</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Fully Effective</span></td>
    <td class="tg-cly1">Nothing more to be done except review and monitor the existing controls. Controls are well designed for the risk, and address the root causes. Management believes they are effective and reliable at all times.</td>
  </tr>
  <tr>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">2</span></td>
    <td class="tg-obmi"><span style="background-color:#D9EAD3">Substantially Effective</span></td>
    <td class="tg-cly1">Most controls are designed correctly and are in place and effective. Some more work to be done to improve operating effectiveness or management has doubts about operational effectiveness and reliability.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">3</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Partially Effective</span></td>
    <td class="tg-cly1">While the design of controls may be largely correct in that they treat most of the root causes of the risk, they are not currently operating very effectively.</td>
  </tr>
  <tr>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">4</span></td>
    <td class="tg-qjco"><span style="background-color:#FCE5CD">Largely Ineffective</span></td>
    <td class="tg-cly1">Significant control gaps. Either controls do not treat root causes or they do not operate at all effectively.</td>
  </tr>
  <tr>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">5</span></td>
    <td class="tg-yfns"><span style="background-color:#F4CCCC">Ineffective</span></td>
    <td class="tg-cly1">Virtually no credible control. Management has no confidence that any degree of control is being achieved due to poor control design or very limited operational effectiveness.</td>
  </tr>
</tbody>
</table>

#### CHER Override

To account for edge case scenarios or other extenuating circumstances that may not be modeled appropriately using the GitLab Observation Management methodology as outlined, the final CHER can be downgraded (i.e move from 2 to 3) at the discretion of the Security Compliance Director if it is determined that the observation's risk rating and therefore CHER does not appropirately reflect the current control or control environment health. The rating cannot be upgraded (i.e move from 4 to 3) to ensure a conservative approach to securing the organization and managing risk.

### Remediation

It is the responsibility of the control owner (or observation owner) to identify the milestones/phases of work involved in the remediation, tracking progress towards remediation, and completing remediation. Once remediation has been completed, the observation owner will notify the Security Compliance team who will validate that remediation has been completed and re-test the observation as appropriate before closing the observation issue.

## Remediation SLA

Observation remediation SLA's are determined by the risk rating of the individual observation. The following table shows the SLA for each risk rating:

| Risk Rating | Remeditation SLA |
| :---: | :---: |
| Low | Best effort |
| Moderate | 90 days, or as otherwise defined by remediation plan |
| High | 30 days, or as otherwise defined by remediation plan |

## Opportunities for Improvement (OFI)

Throughout the course of testing or general monitoring of the GitLab ecosystem, Opportunities for Improvement (OFI) may be identified and documented so that the overall control environment and GitLab's processes can be improved.
 
To capture an OFI, create an issue in the [Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management) project.

### What is the difference between an OFI and an Observation?

- Observations are tied to specific testing attributes and/or reflect areas where a third party compliance professional would be of the opinion that a relevant risk wouldn't be or hasn't been, mitigated.
- OFIs are not tied to specific testing attributes and are general areas of improvement that may streamline compliance or business activities.
- Observations will **always** impact control effectiveness ratings
- OFIs will **never** impact control effectiveness ratings

## Exceptions

Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](https://about.gitlab.com/handbook/engineering/security/#information-security-policy-exception-management-process).

## References

- Parent Policy: [Information Security Policy](/handbook/engineering/security/)
- [GCF Contol Lifecycle](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html#)
- [Sarbanes-Oxley (SOX) Compliance](/handbook/internal-audit/sarbanes-oxley/#sarbanes-oxley-section-404-management-testing-plan)

## Contact

If you have any questions or feedback about the security compliance observation management process please [contact the GitLab security compliance team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html#contact-the-compliance-team).
