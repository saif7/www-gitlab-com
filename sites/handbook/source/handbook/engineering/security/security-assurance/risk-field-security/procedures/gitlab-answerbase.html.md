---
layout: handbook-page-toc
title: "Security Standard Answers Procedures"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[Security Team](/handbook/engineering/security/) > [Field Security Team Page](/handbook/engineering/security/security-assurance/risk-field-security/index.html) > [Field Security Procedures](/handbook/engineering/security/security-assurance/risk-field-security/procedures/index.html)

## Sequence Diagram
```mermaid
sequenceDiagram
    User->>+Issue Board: Submit a new question
    Issue Board-->>+Risk & FS: Alert Analyst
    Risk & FS-->Issue Board: Assign Issue
    Risk & FS-->Issue Board: MR from issue
    Risk & FS-->Issue Board: Draft answer
    Risk & FS->>+SME: Assign MR
    SME-->Risk & FS:Collaborate on Answer
    SME-->Issue Board: Mark MR as "Ready"
    SME->>Risk & FS: Assign MR
    Risk & FS-->Issue Board: Merge MR
    Risk & FS-->Issue Board: Update Original Issue
    Risk & FS->>User: Question Answered
```


## End Users - Ask a New Question - [Video Walkthrough](https://youtu.be/mEhtFnUPJ-Y)
1. Open a new issue on the [GitLab AnswerBase project](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp) using the [new question issue template](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp/-/issues/new?issue%5Btitle%5D=Can%20you%20describe%20...how%20GitLab...%3F&issuable_template=new-question) - Or you can just [Ask a new question](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp/-/issues/new?issue%5Btitle%5D=Can%20you%20describe%20...how%20GitLab...%3F&issuable_template=new-question) using this link
1. Replace the `Title` with a succient human readable version of the question - **Note GitLab Issue Titles can't exceed 255 characters and questions on the issues list should make sense to a human reader**
1. List terms previously searched for in the section `## What Have you Already Searched For` within the Issue Description **Note this helps the Risk and Field Security Team provide a higher quality system**
1. Enter the full text of the question within the section `## Question` in the Issue Description
1. Assign the Issue to yourself
1. Submit the Issue


## End Users - Searching for Questions - [Video Walkthrough](https://youtu.be/H8_w0nQCOtM)
1. Open the issues list on the [GitLab AnswerBase project](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp)
1. Use the Issues Search Functionality to search for question and answer pairs of interest
1. If an issue is [open](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp/-/issues?scope=all&utf8=%E2%9C%93&state=opened) it has not yet been answered or is currently being revised
1. If an issue is [closed](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp/-/issues?scope=all&utf8=%E2%9C%93&state=closed) it has been answered and is not currently being revised

## Subject Matter Experts - Reviewing a Proposed Answer - [Video Walkthrough](https://youtu.be/oxzjq9cWalM)
- Subject matter experts in various areas will be asked to help contribute to question and answer pairs similar to the way they are engaged on assessments currently. 
- Requests for collaboration will be through a GitLab Merge Request Assignment on the [GitLab AnswerBase project](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp)
- Risk and Field Security will make an initial attempt to answer the question with the information avaialable prior to assigning a merge request to a Subject Matter Expert
1. Locate the Assigned Merge Request within the [GitLab AnswerBase project](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp)
1. Review the comments/threads within the merge request
1. Review the proposed answer file
1. Make any changes you feel are necessary to the proposed answer file
1. When you feel the answer is accurately conveyed please mark the Merge Request as `Ready` (i.e. remove from `Draft` or `WIP` status) and reassign the Merge Request to a member of the Risk and Field Security Team

## Subject Matter Experts - Recommending a change - [Video Walkthrough](https://youtu.be/eSEtQHYxZ9E)
- As a Subject Matter Expert, there are times you will be aware of changes before the Risk and Field Security Team is
- As a Subject Matter Expert, you may come across a previously answered question that you feel has an inaccurate answer
- As a Subject Matter Expert, you may come across a question and answer pair that you otherwise have concerns with
1. From the [GitLab AnswerBase project](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp) find the Issue of concern
1. `Re-Open` the Issue from the Issue View
1. Reload the Issue View in your browser (to enable the Create Merge Request from Issue button)
1. Create a Merge Request from the Issue
1. If you know what proposed changes you want to make, open the associated answer file and make changes to the relevant portions
1. If you are unsure what changes you want to make, or just want to ask questions/converse about the question and answer pair use Merge Request Comments and Threads. **Note: Please make use of a Merge Request Thread for concerns that MUST be addressed prior to closing the MR**
1. Follow the procedure for Reviewing a Proposed Answer through completion
